## Xiaomi Firmware Packages For Mi Max Prime (helium)

#### Downloads: [![DownloadSF](https://img.shields.io/badge/Download-SourceForge-orange.svg)](https://sourceforge.net/projects/yshalsager/files/Stable) [![DownloadAFH](https://img.shields.io/badge/Download-AndroidFileHost-brightgreen.svg)](https://www.androidfilehost.com/?w=files&flid=268046)

| ID | MIUI Name | Device Name | Codename |
| --- | --- | --- | --- |
| 302 | MIMAX652 | Xiaomi Mi Max Prime | helium |
| 302 | MIMAX652Global | Xiaomi Mi Max Prime Global | helium |

### XDA Support Thread For helium:
[Go here](https://forum.xda-developers.com/mi-max/development/firmware-xiaomi-mi-max-t3741655)

### XDA Main Thread:
[Go here](https://forum.xda-developers.com/android/software-hacking/devices-yshalsager-t3741446)

#### by [Xiaomi Firmware Updater](https://github.com/XiaomiFirmwareUpdater)
#### Developer: [yshalsager](https://github.com/yshalsager)
